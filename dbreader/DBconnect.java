import java.sql.*;



public class DBconnect {
	private Connection con;
	private Statement st;
	private ResultSet rs;
	public DBconnect(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			createdb();	
			con = DriverManager.getConnection("jdbc:mysql://localhost:3000/bookstore","root","Wasdwasd1");
			st = con.createStatement();

		}catch (Exception ex)
		{
			System.out.println("error in dbconnect: "+ex);
		}
	}
	/*public void getData(){
		try{
			String query = "Select * from persons";
			rs=st.executeQuery(query);
			while(rs.next())
			{
				String name = rs.getString("name");
				String age = rs.getString("name");
			}
		}catch(Exception ex)
		{
			System.out.println("getdata"+ex);
		}

	}*/
	public void insert(String sql)//inserts new employee
	{
		
		try{
			st=con.createStatement();//creates a statement this only has to be called once per method 
			st.executeUpdate(sql);//executes statement
		}catch(Exception ex)
		{
			System.out.println(" insert "+ex);
		}
		
	
	}

	public ResultSet select(String sql)
	{
		try{
			st=con.createStatement();//creates a statement this only has to be called once per method 
			rs= st.executeQuery(sql);
		}catch(Exception ex)
		{
			System.out.println(" insert "+ex);
		}
		return rs;
		
	}
	public void createdb()//creates db
	{
		
			try{
				Class.forName("com.mysql.jdbc.Driver");
				con = DriverManager.getConnection("jdbc:mysql://localhost:3000/","root","Wasdwasd1");
			st=con.createStatement();
			String sql = "CREATE DATABASE BOOKSTORE";
			st.executeUpdate(sql);
			con = DriverManager.getConnection("jdbc:mysql://localhost:3000/bookstore","root","Wasdwasd1");
			st = con.createStatement();
			createTables();
		}catch(Exception ex)
		{
			System.out.println(ex);
		}
		
	
	}
	public void createTables()//creates tables
	{
		
		try{
			st=con.createStatement();
		
			String	sql="CREATE TABLE `customers` ("
					+ "`CustomerID` int(11) NOT NULL AUTO_INCREMENT,"
					+ "`LastName` varchar(15) NOT NULL,"
					+ "`FirstName` varchar(15) NOT NULL,"
					+ "`Phone` varchar(12) DEFAULT NULL,"
					+ "`ShippingAddress` varchar(45) DEFAULT NULL,"
					+ " PRIMARY KEY (`CustomerID`))";
			st.executeUpdate(sql);
			sql="CREATE TABLE `employees` ("
					+ "`EmployeeID` int(11) NOT NULL,"
					+ "`LastName` varchar(15) DEFAULT NULL,"
					+ "`FirstName` varchar(15) DEFAULT NULL,"
					+ "PRIMARY KEY (`EmployeeID`))";
			st.executeUpdate(sql);
		
	
			sql="CREATE TABLE `shippers` ("
					+ "`ShipperID` int(11) ,"
					+ "`ShipperName` varchar(45) DEFAULT NULL,"
					+ " PRIMARY KEY (`ShipperID`))";
			st.executeUpdate(sql);
			sql="CREATE TABLE `subjects` ("
					+ "	  `SubjectID` int(11) NOT NULL,"
					+ " `CategoryName` varchar(15) NOT NULL,"
					+ " PRIMARY KEY (`SubjectID`))";
			st.executeUpdate(sql);
			sql="CREATE TABLE `suppliers` ("
					+ " `SupplierID` int(11) NOT NULL,"
					+ "`CompanyName` varchar(45) DEFAULT NULL,"
					+ "`ContactLastName` varchar(15) NOT NULL,"
					+ "`ContactFirstName` varchar(15) NOT NULL,"
					+ "`Phone` varchar(12) ,"
					+ "PRIMARY KEY (`SupplierID`))";
			st.executeUpdate(sql);
			 sql = "CREATE TABLE `books` ("
					+ "`BookID` int(11) NOT NULL AUTO_INCREMENT,"
					+ "`Title` varchar(45) NOT NULL,"
					+ "  `Unit_Price` double unsigned NOT NULL,"
					+ "`Author` varchar(45) DEFAULT NULL,"
					+ "`Unit_in_Stock` int(10) unsigned NOT NULL,"
					+ "`SupplierID` int(11) DEFAULT NULL,"
					+ "`SubjectID` int(11) DEFAULT NULL,"
					+ "PRIMARY KEY (`BookID`),"
					+ "KEY `SupplierID` (`SupplierID`),"
					+ "KEY `SubjectID` (`SubjectID`),"
					+ "CONSTRAINT `books_ibfk_1` FOREIGN KEY (`SupplierID`) REFERENCES `suppliers` (`SupplierID`),"
					+ "CONSTRAINT `books_ibfk_2` FOREIGN KEY (`SubjectID`) REFERENCES `subjects` (`SubjectID`))";
			st.executeUpdate(sql);
			sql="CREATE TABLE `orders` ( "
					+ "`OrderID` int(11) NOT NULL AUTO_INCREMENT ,"
					+ "`CustomerID` int(11) NOT NULL,"
					+ "`EmployeeID` int(11) DEFAULT NULL,"
					+ "`OrderDate` varchar(12) DEFAULT NULL,"
					+ "`ShippedDate` varchar(12) DEFAULT NULL,"
					+ "`ShipperID` int(11) DEFAULT NULL,"
					+ "PRIMARY KEY (`OrderID`),"
					+ "KEY `CustomerID` (`CustomerID`),"
					+ "KEY `EmployeeID` (`EmployeeID`),"
					+ "KEY `ShipperID` (`ShipperID`),"
					+ "CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`CustomerID`) REFERENCES `customers` (`CustomerID`),"
					+ "CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`EmployeeID`) REFERENCES `employees` (`EmployeeID`),"
					+ "CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`ShipperID`) REFERENCES `shippers` (`ShipperID`))";
			st.executeUpdate(sql);
			sql="CREATE TABLE `orderdetails` ("
					+ "`BookID` int(11) NOT NULL,"
					+ "`OrderID` int(11) NOT NULL,"
					+ "`Quantity` int(11) DEFAULT NULL,"
					+ "PRIMARY KEY (`BookID`,`OrderID`),"
					+ "KEY `OrderID` (`OrderID`),"
					+ "CONSTRAINT `orderdetails_ibfk_1` FOREIGN KEY (`BookID`) REFERENCES `books` (`BookID`),"
					+ "CONSTRAINT `orderdetails_ibfk_2` FOREIGN KEY (`OrderID`) REFERENCES `orders` (`OrderID`))";
			st.executeUpdate(sql);
			
			//suppliers
			sql= "insert into bookstore.suppliers values (1,'Amazon','Hamilton','Laurell','605-145-1875')";
			st.executeUpdate(sql);
			sql= "insert into bookstore.suppliers values (2,'Ebay','Koontz','Dean','6052441104')";
			st.executeUpdate(sql);
			sql= "insert into bookstore.suppliers values (3,'Booksamillion','Roberts','Nora','201-787-3320')";
			st.executeUpdate(sql);
			sql= "insert into bookstore.suppliers values (4,'University','Carter','Stephen','229-412-2004')";
			st.executeUpdate(sql);

			
			//subjects
			sql= "insert into bookstore.subjects values (1,'Fiction')";
			st.executeUpdate(sql);
			sql= "insert into bookstore.subjects values (2,'History')";
			st.executeUpdate(sql);
			sql= "insert into bookstore.subjects values (3,'Travel')";
			st.executeUpdate(sql);
			sql= "insert into bookstore.subjects values (4,'Technology')";
			st.executeUpdate(sql);
			
			//books
			sql= "insert into bookstore.books values (1,'The Quickle',15.94,'James',5,3,1)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.books values (2,'Blaze',13.24,'Richard',2,3,1)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.books values (3,'the Navigator',14.01,'Clive',10,2,1)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.books values (4,'Birmingham',19.99,'Tim',12,3,2)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.books values (5,'North Carolina Ghost',7.95,'Lynne',5,2,2)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.books values (6,'Why I Still live in Louisiana',5.95,'Ellen',30,1,3)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.books values (7,'the World is Flat',30,'Thomas',17,3,4)";
			st.executeUpdate(sql);
			//Employees
			sql= "insert into bookstore.employees values (1,'Larson','Erik')";
			st.executeUpdate(sql);
			sql= "insert into bookstore.employees values (2,'Steely','John')";
			st.executeUpdate(sql);

			//shippers
			sql= "insert into bookstore.shippers values (1,'UPS')";
			st.executeUpdate(sql);
			sql= "insert into bookstore.shippers values (2,'USPS')";
			st.executeUpdate(sql);
			sql= "insert into bookstore.shippers values (3,'FedEx')";
			//sql="insert into bookstore.shippers values (null,'NOTSHIPPED')";
			st.executeUpdate(sql);
			
			//customers
			sql= "insert into bookstore.customers values (1,'Lee','James','229-541-4568','214 Valdosta Rd., Valdosta, GA, 31605')";
			st.executeUpdate(sql);
			sql= "insert into bookstore.customers values (2,'Smith','John','334-057-0087','140 Magnolia St., Auburn, GA, 36830')";
			st.executeUpdate(sql);
			sql= "insert into bookstore.customers values (3,'See','Lisa','605-054-0010','411 Maple St., Sioux Falls, SD, 57104')";
			st.executeUpdate(sql);
			sql= "insert into bookstore.customers values (4,'Collins','Jackie','605-044-6582','321 W. Avenue, Sioux Falls, SD, 57104')";
			st.executeUpdate(sql);
			
			//orders
			sql= "insert into bookstore.orders values (1,1,1,'08/01/13','08/03/13',1)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.orders values (2,1,2,'08/04/13',null,null)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.orders values (3,2,1,'08/01/13','08/03/13',2)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.orders values (4,4,2,'08/04/13','08/05/13',1)";
			st.executeUpdate(sql);
			
			//orderDetails
			sql= "insert into bookstore.orderDetails values (1,1,2)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.orderDetails values (4,1,1)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.orderDetails values (6,2,2)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.orderDetails values (7,2,3)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.orderDetails values (5,3,1)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.orderDetails values (3,4,1)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.orderDetails values (4,4,1)";
			st.executeUpdate(sql);
			sql= "insert into bookstore.orderDetails values (7,4,1)";
			st.executeUpdate(sql);
			
			//
		}catch(Exception ex)
		{
			System.out.println(" in create tables "+ex);
		}
	
	}
}

