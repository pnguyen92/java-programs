import java.sql.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.io.BufferedReader;
import org.json.JSONObject;
import org.json.JSONArray;//import org.json.simple.JSONArray;
public class dbreader {
	static Scanner scan = new Scanner(System.in);
	static DBconnect connect = new DBconnect();
	static ResultSet r;
	static ResultSet rs;
	double amount = 0.0;
	double sum = 0.0;//C:\\Users\\matt\\Documents\\javajar\\
	int count = 1;
	static String sql = "";
	//private static final String filePath = "C:\\Users\\matt\\Documents\\database2\\project\\json.txt";
	private static final String filePath = "C:\\Users\\matt\\Documents\\database2\\project\\yelp_dataset_challenge_academic_dataset.txt";
	public static void main(String[] args) throws Exception {
		/*
		sql = "SELECT distinct o.OrderID,c.FirstName,e.FirstName,o.OrderDate,o.ShippedDate,o.ShipperID From "
				+ "bookstore.orders as o,bookstore.customers as c,bookstore.employees as e ,bookstore.shippers as s "
				+ "WHERE o."
				+ type
				+ "="
				+ id
				+ " and c.CustomerID=o.CustomerID and e.EmployeeID=o.EmployeeID ";
				sql= "insert into bookstore.suppliers values (1,'Amazon','Hamilton','Laurell','605-145-1875')";
		// System.out.println(sql);
		rs = connect.select(sql);
		*/
		BufferedReader br = null;
		String sCurrentLine;
		String bid;
		String uid;
		String rid;
		String date;
		String text;
		String full_address;
		String city;
		int review_count;
		String name;
		double longitude;
		double avg;
		double rcount;
		String state;
		double stars;
		double latitude;
		String type;
		JSONObject temp,inner;
		boolean open;
		Object days;
		JSONArray arr;
		String sql="";
		Iterator<?> keys;
		br = new BufferedReader(new FileReader(filePath));
			
			while ((sCurrentLine = br.readLine()) != null) {
				try{
					String s =sCurrentLine;
			        JSONObject jsonObject = new JSONObject(s);
			        
			       
   			       if (jsonObject.has("full_address")){//for business insert
   			       		bid = (String) jsonObject.get("business_id");
	   			      	full_address = (String) jsonObject.get("full_address");
				        city = (String) jsonObject.get("city");
				        review_count = (int) jsonObject.get("review_count");
				        name = (String) jsonObject.get("name");
				        longitude = (double) jsonObject.get("longitude");
				        state = (String) jsonObject.get("state");
				        stars = (double) jsonObject.get("stars");
				        latitude = (double) jsonObject.get("latitude");
	   			        type = (String) jsonObject.get("type");
	   			        open = (boolean)  jsonObject.get("open");
	   			        sql= "insert into yelp.business(business_id,full_address,city,review_count,name,longitude,state,stars,latitude,type,open) values (\""+bid+"\",\""+full_address+"\",\""+city+"\","+review_count+",\""+name+"\","+longitude+",\""+state+"\",\""+stars+"\",\""+latitude+"\",\""+type+"\","+open+")";
	   			        //System.out.println(sql);
	   			        connect.insert(sql);
						//hours json object
				        days =  jsonObject.get("hours");
						temp = (JSONObject) days;
						keys = temp.keys();
				        while( keys.hasNext() ){//keys is a json object
				            String key = (String)keys.next();
				            if( temp.get(key) instanceof JSONObject ){
				            	 inner= (JSONObject)temp.get(key);
				            	sql= "insert into yelp.hours(business_id,day,open,close) values (\""+bid+"\",\""+key+"\",\""+inner.get("open")+"\",\""+inner.get("close")+"\")";
				            	//System.out.println(sql);
				            	connect.insert(sql);
				            }
				        }
				        
				         //array
				        //String categories = (String) jsonObject.get("categories"); 
						arr =  jsonObject.getJSONArray("categories");
						for (int i = 0 ; i < arr.length(); i++) {
							sql= "insert into yelp.categories(business_id,category) values (\""+bid+"\",\""+arr.getString(i)+"\")";
				            //System.out.println(sql);//this is the day
				            connect.insert(sql);
				        }
				         
						temp = (JSONObject) jsonObject.get("attributes");
						keys = temp.keys();
				        while( keys.hasNext() ){
				            String key = (String)keys.next();
				            if( temp.get(key) instanceof JSONObject ){
				            	 //may do this later
				            	 //inner= (JSONObject)temp.get(key);
				            	// System.out.println(key+":   "+inner);
				            	//sql= "insert into yelp.hours(business_id,day,open,close) values ('"+bid+"','"+key+"','"+inner.get("open")+"','"+inner.get("close")+"')";
				            	
				            }else{
				            	sql= "insert into yelp.attributes(business_id,attribute,value) values (\""+bid+"\",\""+key+"\",\""+temp.get(key)+"\")";
				            	//System.out.println(sql);
				            	connect.insert(sql);
				            }
				        }

				        //array String neighborhoods = (String) jsonObject.get("neighborhoods");
				        arr =  jsonObject.getJSONArray("neighborhoods");
						for (int i = 0 ; i < arr.length(); i++) {
							sql= "insert into yelp.neighborhoods(business_id,neighborhood) values (\""+bid+"\",\""+arr.getString(i)+"\")";
							connect.insert(sql);
				            //System.out.println(sql);//this is the day
				        }
				    }
				   else if (jsonObject.has("votes") && jsonObject.has("review_id")){//review
						bid = (String) jsonObject.get("business_id");
						rid = (String) jsonObject.get("review_id");
						uid = (String) jsonObject.get("user_id");
						stars = (int) jsonObject.get("stars");
						date = (String) jsonObject.get("date");
						text = (String) jsonObject.get("text");
						type = (String) jsonObject.get("type");
						sql= "insert into yelp.review(bid,rid,uid,stars,date,text,type) values (\""+bid+"\",\""+rid+"\",\""+uid+"\","+stars+",\""+date+"\",\""+text+"\",\""+type+"\")";
						connect.insert(sql);
					}
					else if (jsonObject.has("yelping_since") && jsonObject.has("friends")){//user
						date = (String) jsonObject.get("yelping_since");
						rcount =(int)  jsonObject.get("review_count");
						uid = (String) jsonObject.get("user_id");
						name = (String) jsonObject.get("name");
						avg = (double) jsonObject.get("average_stars");
						type = (String) jsonObject.get("type");
						sql= "insert into yelp.user(datecreated,rcount,uid,name,ravg,type) values (\""+date+"\",\""+rcount+"\",\""+uid+"\",\""+name+"\",\""+avg+"\",\""+type+"\")";
						connect.insert(sql);
						arr =  jsonObject.getJSONArray("friends");
						for (int i = 0 ; i < arr.length(); i++) {
							sql= "insert into yelp.friends(uid,fid) values (\""+uid+"\",\""+arr.getString(i)+"\")";
							connect.insert(sql);
				        }
					}
			
				}catch (Exception ex){
					System.out.println(ex.toString());
					//System.out.println(sql);
				}
			}
			
	}
}
 class DBconnect {
	private Connection con;
	private Statement st;
	private ResultSet rs;
	public DBconnect(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3000/bookstore","root","Wasdwasd!");
			st = con.createStatement();

		}catch (Exception ex)
		{
			System.out.println("error in dbconnect: "+ex);
		}
	}

	public void insert(String sql)//inserts new employee
	{
		
		try{
			st=con.createStatement();//creates a statement this only has to be called once per method 
			st.executeUpdate(sql);//executes statement
		}catch(Exception ex)
		{
			System.out.println(" insert "+ex);
				System.out.println(sql);
				System.out.println();
				System.out.println();
		}
	}

	public ResultSet select(String sql)
	{
		try{
			st=con.createStatement();//creates a statement this only has to be called once per method 
			rs= st.executeQuery(sql);
		}catch(Exception ex)
		{
			System.out.println(" select "+ex);
		}
		return rs;
		
	}
	public void createdb()//creates db
	{
		
			try{
				Class.forName("com.mysql.jdbc.Driver");
				con = DriverManager.getConnection("jdbc:mysql://localhost:3000/","root","Wasdwasd1");
			st=con.createStatement();
			String sql = "CREATE DATABASE yelp;USE yelp";
			st.executeUpdate(sql);
			con = DriverManager.getConnection("jdbc:mysql://localhost:3000/bookstore","root","Wasdwasd1");
			st = con.createStatement();
			createTables();
		}catch(Exception ex)
		{
			System.out.println(ex);
		}
		
	
	}
	public void createTables()//creates tables
	{
		
		try{
			st=con.createStatement();
			String	sql="CREATE TABLE `attributes` ("
					+ "`id` int(11) NOT NULL AUTO_INCREMENT,"
					+ " `business_id` varchar(45) DEFAULT NULL,"
					+ "`attribute` varchar(75) DEFAULT NULL,"
					+ "`value` varchar(45) DEFAULT NULL,"
					+ " PRIMARY KEY (`id`)"
					+ ") ENGINE=InnoDB AUTO_INCREMENT=55374 DEFAULT CHARSET=utf8 ;";
			st.executeUpdate(sql);
			sql="CREATE TABLE `business` ("
				 +" `id` int(11) NOT NULL AUTO_INCREMENT,"
				 +" `business_id` varchar(75) NOT NULL,"
				 +" `full_address` varchar(100) DEFAULT NULL,"
				 +" `open` bit(1) DEFAULT NULL,"
				 +" `city` varchar(75) DEFAULT NULL,"
				 +" `review_count` int(11) DEFAULT NULL,"
				 +" `name` varchar(100) DEFAULT NULL,"
				 +" `state` varchar(45) DEFAULT NULL,"
				 +" `stars` varchar(45) DEFAULT NULL,"
				 +" `latitude` float DEFAULT NULL,"
				 +" `longitude` float DEFAULT NULL,"
				 +" `type` varchar(45) DEFAULT NULL,"
				+"  PRIMARY KEY (`id`)"
				+") ENGINE=InnoDB AUTO_INCREMENT=7307 DEFAULT CHARSET=utf8;";

			st.executeUpdate(sql);
		
	
			sql="CREATE TABLE `categories` ("
				+"  `id` int(11) NOT NULL AUTO_INCREMENT,"
				+"  `business_id` varchar(75) DEFAULT NULL,"
				+"  `category` varchar(75) DEFAULT NULL,"
				+"  PRIMARY KEY (`id`)"
				+") ENGINE=InnoDB AUTO_INCREMENT=26647 DEFAULT CHARSET=utf8;";
			st.executeUpdate(sql);
			sql="CREATE TABLE `hours` ("
				 +" `id` int(11) NOT NULL AUTO_INCREMENT,"
				 +" `business_id` varchar(45) DEFAULT NULL,"
				 +" `day` varchar(45) DEFAULT NULL,"
				 +" `open` varchar(45) DEFAULT NULL,"
				 +" `close` varchar(45) DEFAULT NULL,"
				+"  PRIMARY KEY (`id`)"
				+") ENGINE=InnoDB AUTO_INCREMENT=32926 DEFAULT CHARSET=utf8;";
			st.executeUpdate(sql);
			sql="CREATE TABLE `neighborhoods` ("
				 +" `id` int(11) NOT NULL AUTO_INCREMENT,"
				+"  `business_id` varchar(45) DEFAULT NULL,"
				+"  `neighborhood` varchar(45) DEFAULT NULL,"
				+"  PRIMARY KEY (`id`)"
				+") ENGINE=InnoDB AUTO_INCREMENT=1324 DEFAULT CHARSET=utf8;";
			st.executeUpdate(sql);
		}catch(Exception ex)
		{
			System.out.println(" in create tables "+ex);
		}
	}
}

