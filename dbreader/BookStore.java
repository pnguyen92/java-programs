import java.sql.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.io.BufferedReader;
import org.json.JSONObject;
import org.json.JSONArray;//import org.json.simple.JSONArray;
public class dbreader {
	static Scanner scan = new Scanner(System.in);
	static DBconnect connect = new DBconnect();
	static ResultSet r;
	static ResultSet rs;
	double amount = 0.0;
	double sum = 0.0;//C:\\Users\\matt\\Documents\\javajar\\
	int count = 1;
	static String sql = "";
	//private static final String filePath = "C:\\Users\\matt\\Documents\\database2\\project\\json.txt";
	private static final String filePath = "C:\\Users\\matt\\Documents\\database2\\project\\yelp_dataset_challenge_academic_dataset.txt";
	public static void main(String[] args) throws Exception {
		/*
		sql = "SELECT distinct o.OrderID,c.FirstName,e.FirstName,o.OrderDate,o.ShippedDate,o.ShipperID From "
				+ "bookstore.orders as o,bookstore.customers as c,bookstore.employees as e ,bookstore.shippers as s "
				+ "WHERE o."
				+ type
				+ "="
				+ id
				+ " and c.CustomerID=o.CustomerID and e.EmployeeID=o.EmployeeID ";
				sql= "insert into bookstore.suppliers values (1,'Amazon','Hamilton','Laurell','605-145-1875')";
		// System.out.println(sql);
		rs = connect.select(sql);
		*/
		BufferedReader br = null;
		String sCurrentLine;
		String bid;
		String full_address;
		String city;
		int review_count;
		String name;
		double longitude;
		String state;
		double stars;
		double latitude;
		String type;
		JSONObject temp,inner;
		boolean open;
		Object days;
		JSONArray arr;
		String sql="";
		Iterator<?> keys;
		br = new BufferedReader(new FileReader(filePath));
			
			while ((sCurrentLine = br.readLine()) != null) {
				try{
					String s =sCurrentLine;
			        JSONObject jsonObject = new JSONObject(s);
			        bid = (String) jsonObject.get("business_id");
			        full_address = (String) jsonObject.get("full_address");
			        city = (String) jsonObject.get("city");
			        review_count = (int) jsonObject.get("review_count");
			        name = (String) jsonObject.get("name");
			        longitude = (double) jsonObject.get("longitude");
			        state = (String) jsonObject.get("state");
			        stars = (double) jsonObject.get("stars");
			        latitude = (double) jsonObject.get("latitude");
   			        type = (String) jsonObject.get("type");
   			        open = (boolean)  jsonObject.get("open");
   			        sql= "insert into yelp.business(business_id,full_address,city,review_count,name,longitude,state,stars,latitude,type,open) values ('"+bid+"','"+full_address+"','"+city+"',"+review_count+",'"+name+"',"+longitude+",'"+state+"','"+stars+"','"+latitude+"','"+type+"',"+open+")";
   			        //System.out.println(sql);
   			        connect.insert(sql);
					//hours json object
			        days =  jsonObject.get("hours");
					temp = (JSONObject) days;
					keys = temp.keys();
			        while( keys.hasNext() ){
			            String key = (String)keys.next();
			            if( temp.get(key) instanceof JSONObject ){
			            	 inner= (JSONObject)temp.get(key);
			            	sql= "insert into yelp.hours(business_id,day,open,close) values ('"+bid+"','"+key+"','"+inner.get("open")+"','"+inner.get("close")+"')";
			            	//System.out.println(sql);
			            	connect.insert(sql);
			            }
			        }
			        
			         //array
			        //String categories = (String) jsonObject.get("categories"); 
					arr =  jsonObject.getJSONArray("categories");
					for (int i = 0 ; i < arr.length(); i++) {
						sql= "insert into yelp.categories(business_id,category) values ('"+bid+"','"+arr.getString(i)+"')";
			            //System.out.println(sql);//this is the day
			            connect.insert(sql);
			        }
			         
					temp = (JSONObject) jsonObject.get("attributes");
					keys = temp.keys();
			        while( keys.hasNext() ){
			            String key = (String)keys.next();
			            if( temp.get(key) instanceof JSONObject ){
			            	 //may do this later
			            	 //inner= (JSONObject)temp.get(key);
			            	// System.out.println(key+":   "+inner);
			            	//sql= "insert into yelp.hours(business_id,day,open,close) values ('"+bid+"','"+key+"','"+inner.get("open")+"','"+inner.get("close")+"')";
			            	
			            }else{
			            	sql= "insert into yelp.attributes(business_id,attribute,value) values ('"+bid+"','"+key+"','"+temp.get(key)+"')";
			            	//System.out.println(sql);
			            	connect.insert(sql);
			            }
			        }

			        //array String neighborhoods = (String) jsonObject.get("neighborhoods");
			        arr =  jsonObject.getJSONArray("neighborhoods");
					for (int i = 0 ; i < arr.length(); i++) {
						sql= "insert into yelp.neighborhoods(business_id,neighborhood) values ('"+bid+"','"+arr.getString(i)+"')";
						connect.insert(sql);
			            //System.out.println(sql);//this is the day
			        }
				}catch (Exception ex){
					System.out.println(ex.toString());
					System.out.println(sql);
				}
			}
	}
}
 class DBconnect {
	private Connection con;
	private Statement st;
	private ResultSet rs;
	public DBconnect(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3000/bookstore","root","Wasdwasd!");
			st = con.createStatement();

		}catch (Exception ex)
		{
			System.out.println("error in dbconnect: "+ex);
		}
	}

	public void insert(String sql)//inserts new employee
	{
		
		try{
			st=con.createStatement();//creates a statement this only has to be called once per method 
			st.executeUpdate(sql);//executes statement
		}catch(Exception ex)
		{
			System.out.println(" insert "+ex);
		}
	}

	public ResultSet select(String sql)
	{
		try{
			st=con.createStatement();//creates a statement this only has to be called once per method 
			rs= st.executeQuery(sql);
		}catch(Exception ex)
		{
			System.out.println(" select "+ex);
		}
		return rs;
		
	}
}

