/*	CS4345
	Fall 2014
	Programming 1
	Phuong Nguyen  */

import java.net.*;
import java.io.*;
import java.util.*;

public class MyClient{
	public static void main(String[] args){
		try{
			//create a socket to make connection to server socket
			Socket sock = new Socket("127.0.0.1", 7000);
			
			//create an output stream to send data to the server
			DataOutputStream data2server = new DataOutputStream(sock.getOutputStream());
			//create an input stream to receive data from server
			DataInputStream result4mserver = new DataInputStream(sock.getInputStream());

			Scanner inp = new Scanner(System.in);
			int max =10, count=0;
			
			while(count<max){
				System.out.print("Enter a message: ");
				String str= inp.nextLine();
			
				//send the data to the server
				data2server.writeUTF(str);
				data2server.flush();
			
				//receive the result from the server	
				String result = result4mserver.readUTF();
				System.out.println("Server says: " + result);
				count++;
			}
			sock.close();
		 } catch(IOException ioe){
				System.err.println(ioe);
			}
	}//End-of-main
}//End-ofclass