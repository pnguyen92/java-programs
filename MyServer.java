/*	CS4345
	Fall 2014
	Programming 1
	Phuong Nguyen  */

import java.net.*;
import java.io.*;
import java.util.*;

public class MyServer{
	public static void main(String[] args){
		try{
			ServerSocket servSock = new ServerSocket(7000);
			System.out.println("Server started at "+ new Date() + '\n');
			
			//Listen for a connection request
			Socket sock = servSock.accept();
			
			//create data input and data output streams
			DataInputStream input4mclient = new DataInputStream(sock.getInputStream());
			DataOutputStream output2client = new DataOutputStream(sock.getOutputStream());

			int max=10, count=0;

			while(count<max){
				//receive data from client
				String str = input4mclient.readUTF();
				System.out.println("Client says: " + str);

				//write something back.
				Scanner inp = new Scanner(System.in);
				System.out.print("Enter a message: ");
				String line= inp.nextLine();

				//send result back to client
				output2client.writeUTF(line);
				output2client.flush();
				count++;
			}
			sock.close();
			
		 } catch(IOException ioe){
				System.err.println(ioe);
			}
	}//End-of-main
}//End-of-class
