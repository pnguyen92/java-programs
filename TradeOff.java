/*	CS4345
	Fall 2014
	Programming 2
	Phuong Nguyen  */

import java.util.*;

public class TradeOff{
	public static void main(String[] args){
		int[] array= new int[50];
		int number=0, count=0;
		Random rand = new Random();

		for(int i=0; i<array.length; i++){
			number = rand.nextInt(50)+1;
			array[i] = number;
			count++;
			if(i==0)	
				System.out.print("Original order:	   [" + array[i] + ", ");
			else if(i==array.length-1)
				System.out.print(array[i] +"]--> " + array.length + "\n");
			else
				System.out.print(array[i] + ", ");
		}
		standardMergesort(array);
		threadedMergesort(array);
	}
	public static void standardMergesort(int[] array){
		long start = System.nanoTime();
		int size = array.length;
		int mid=size/2;
		int i=0,j=0, k=0;
		int[] first = new int[mid];
		int[] second = new int[size-mid];
		//int[] finalArray = new int[size];
		System.arraycopy(array, 0, first, 0, first.length);
        System.arraycopy(array, first.length, second, 0, second.length);
		Arrays.sort(first);
		Arrays.sort(second);
		while(i<first.length && j<second.length){
			if (first[i] <= second[j]){
				array[k]=first[i];
				i++;
			}
			else{
				array[k]=second[j];
				j++;
			}
			k++;
		}
		while(i<first.length){
			array[k]= first[i];
			k++;
			i++;

		}
		while(j<second.length){
			array[k]= second[j];
			k++;
			j++;

		}
		for(i=0; i <array.length; i++){
			if(i==0)
				System.out.print("Standard Mergesort:[" + array[i] + ", ");
			else if(i==array.length-1)
				System.out.print(array[i] +"]--> " + array.length + "\n");
			else
				System.out.print(array[i] + ", ");
		}
		long time = System.nanoTime() - start;
		System.out.println("Time: " + time + "ms");
		//System.out.println(15/2);
	}
	public static void threadedMergesort(int[] array){
		long start = System.nanoTime();
		int size = array.length;
		int mid=size/2;
		ArrayList<Integer> subArray1 = new ArrayList<Integer>();
		ArrayList<Integer> subArray2 = new ArrayList<Integer>();
		ArrayList<Integer> finalArray = new ArrayList<Integer>();

		for(int i=0; i<size; i++ ){
			if(size%2==0){
				if(i<(size/2))
					subArray1.add(array[i]);
				else
					subArray2.add(array[i]);
			}
			else{
				if(i<(size-1)/2)
					subArray1.add(array[i]);
				else
					subArray2.add(array[i]);
			}
		}
		sortThread t1 = new sortThread(subArray1);
		sortThread t2 = new sortThread(subArray2);

		t1.start();					t2.start();
		subArray1=t1.getArray();	subArray2=t2.getArray();

		try{
			t1.join();	
			t2.join();	
		} catch (InterruptedException ex){
			ex.printStackTrace();
		}

		mergeThread t3 = new mergeThread(subArray1, subArray2);
		t3.start();

		try{
			t1.join();
			t2.join();
			t3.join();		
		} catch (InterruptedException ex){
			ex.printStackTrace();
		}
		long time = System.nanoTime() - start;
		System.out.println("Time: " + time + "ms");
	}
}

class sortThread extends Thread{
	ArrayList<Integer> array = new ArrayList<Integer>();

	public sortThread(ArrayList<Integer> temp) { array=temp; }
	public void run(){
		Collections.sort(array);
	}
	public ArrayList<Integer> getArray(){
		return array; 
	}
}

class mergeThread extends Thread{
	ArrayList<Integer> a1 = new ArrayList<Integer>();
	ArrayList<Integer> a2 = new ArrayList<Integer>();
	

	public mergeThread(ArrayList<Integer> temp1, ArrayList<Integer> temp2) { 
		a1=temp1;	a2=temp2; 
	}
	public void run(){
		Integer[] list1 = a1.toArray(new Integer[0]);
		Integer[] list2 = a2.toArray(new Integer[0]);
		int size = list1.length + list2.length;
		ArrayList<Integer> a3 = new ArrayList<Integer>();
		Integer[] list3 = a3.toArray(new Integer[size]);
		int i=0, j=0, k=0;
		while(i<list1.length && j<list2.length){
			if (list1[i] <= list2[j]){
				list3[k]=list1[i];
				i++;
			}
			else{
				list3[k]=list2[j];
				j++;
			}
			k++;
		}
		while(i<list1.length){
			list3[k]= list1[i];
			k++;
			i++;

		}
		while(j<list2.length){
			list3[k]= list2[j];
			k++;
			j++;

		}

		for(i=0; i <list3.length; i++){
			if(i==0)
				System.out.print("Threaded Mergesort:[" + list3[i] + ", ");
			else if(i==list3.length-1)
				System.out.print(list3[i] +"]--> " + list3.length + "\n");
			else
				System.out.print(list3[i] + ", ");
		}
	}
}